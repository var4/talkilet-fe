Application to learn Hungarian and Finnish languages.

Application is writen in mobile-first approach. Available for mobile, tab and desktop devices.

*Made with love and with Create React App.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.


