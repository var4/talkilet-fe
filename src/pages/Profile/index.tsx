import React from 'react';
import { useCurrentUser } from '../../shared/api/swr/be-data';
import { LoginPage } from '../LoginPage';

export function Profile() {
  const { data: user, error: userError } = useCurrentUser();

  if (!user || userError) {
    return <LoginPage />;
  }

  return <div> Profile settings Page</div>;
}
