import React from 'react';
import classNames from 'classnames';
import styles from './styles.module.scss';
import image from '../../assets/top_part_picture.jpg';
import secPart from '../../assets/second_part.jpg';
import thirdPart from '../../assets/third_part.jpg';

export function HomePage() {

  const buttonPrimaryStyles = classNames({
    [styles.button]: true,
    [styles.primary]: true,
  });
  const buttonSecondaryStyles = classNames({
    [styles.button]: true,
    [styles.secondary]: true,
  });
  const evenPart = classNames({
    [styles.part]: true,
    [styles.secondarySandBG]: true,
  });

  return (
    <div className={styles.container}>
      <div className={styles.part}>
        <h1 className={styles.title}>Easy learn of difficult language!</h1>
        <img src={image} alt="" className={styles.img} />

        <div className={styles.buttonContainer}>
          <button type="button" className={buttonPrimaryStyles}>
            Start now
          </button>
          <button type="button" className={buttonSecondaryStyles}>
            Register
          </button>
        </div>
      </div>

      <div className={evenPart}>
        <p className={styles.paragraph}>
          Scentific research proves that learning of foreign language improves your brain and
          communication skill.
        </p>
        <img src={secPart} alt="" className={styles.img} />
      </div>

      <div className={styles.part}>
        <p className={styles.paragraph}>
          Why Hungarian? This language makes you unique. On the whole Earth there is only 15 mln
          people who speak Hungarian. Also, this language is in top-5 most hardest languages on
          whole planet.
        </p>
        <img src={thirdPart} className={styles.img} alt="" />
      </div>
    </div>
  );
}
