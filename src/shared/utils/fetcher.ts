import { CustomError } from '../../models/customError';

export const fetcher = async (url: string) => {
  const response = await fetch(url);
  if (!response.ok) {
    const error = CustomError();
    error.status = response.status;
    error.path = url;
    throw error;
  }

  const data = await response.json();
  return data;
};
