import { useEffect } from "react";

export function useModalAnimationHelper(modalContainer: HTMLElement | null, isOpen: boolean) {
    useEffect(() => {
        let delayedExecution: ReturnType<typeof setTimeout>;
        if (isOpen) {
            document.body.style.overflow = 'hidden';
            if(modalContainer) {
                modalContainer.style.zIndex = "101";
            
            }
        } else if (modalContainer) {
            delayedExecution = setTimeout(() => {
                modalContainer.style.zIndex = "-1"
            }, 800)
        }
        return () => clearTimeout(delayedExecution)
    }, [isOpen, modalContainer])
}