import useSWR from 'swr';
import { fetcher } from '../../utils/fetcher';
import { env } from '../../utils/env/env';

export function useCurrentUser() {
  const { data, error, mutate } = useSWR(`${env.apiUrl}/user?userId=1`, fetcher, {
    revalidateOnFocus: false,
  });
  return { data, error, mutate };
}

export function useDictionary() {
  const { data, error, mutate } = useSWR(`${env.apiUrl}/dictionary?dictionaryId=1`, fetcher, {
    revalidateOnFocus: false,
  });
  return { data, error, mutate };
}

export function useWordsByDictionaryName(name: string) {
  const { data, error, mutate } = useSWR(`${env.apiUrl}/words?tableName=${name}`, fetcher, {
    revalidateOnFocus: false,
  });
  return { data, error, mutate };
}

