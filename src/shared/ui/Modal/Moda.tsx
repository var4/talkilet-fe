import React, { ReactElement } from "react";
import styles from './styles.module.scss';
import closeSvg from '../../../assets/close.svg';
import { useModalAnimationHelper } from "../../utils/hooks/useModalAnimationHelper";
import classNames from "classnames";

interface IModalProps {
    children: ReactElement;
    isOpen: boolean;
    id: string;
    closeHandler: (isOpen: boolean) => void;
    title: string;
}
export function Modal({children, isOpen, id, closeHandler, title}: IModalProps): ReactElement | null {
    const modalContainer = document.getElementById(id);
    useModalAnimationHelper(modalContainer, isOpen);

    const modalContainerStyles = classNames({
        [styles.modalContainer]: true,
        [styles.modalContainerOpen]: isOpen,
    });

    const modalStyles = classNames({
        [styles.modal]: true,
        [styles.modalOpen]: isOpen,
    });

    return (
        <div className={modalContainerStyles} id={id}>
            <div className={styles.backdrop}/>
            <div className={modalStyles} role="dialog" aria-modal="true">
                <div className={styles.close}>
                    <h2>{title}</h2>
                    <img src={closeSvg} alt="" className={styles.closeBtn}/>
                </div>
                {children}
            </div>
        </div>
    )
}