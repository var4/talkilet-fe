import React from 'react';
import styles from './styles.module.scss';
import logo from '../../assets/logo_t_e.jpg';

export function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.rightPart}>
        <img src={logo} alt="" className={styles.logo} />
      </div>
      <div className={styles.leftPart}>
        <ul className={styles.list}>
          <li className={styles.listItem}>Contact us</li>
          <li className={styles.listItem}>Follow us on:</li>
        </ul>
      </div>
    </footer>
  );
}
