import React, { useState } from 'react';
import styles from './styles.module.scss';
import logo from '../../../assets/logo_t_e_croped.jpg';
import avatar from '../../../assets/astronaut-colored.svg';
import { Navigation } from './Navigation/ui';

export function Header() {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <header className={styles.headerContainer}>
      <div className={styles.headerWrapper}>
        <div className={styles.leftSide}>
          <img src={logo} alt="" className={styles.logo} />
        </div>
        <div className={styles.rightSide}>
          <img src={avatar} alt="" className={styles.avatar} />
          <div className={styles.menuMobile}>
            <button
              className={!isMenuOpen ? styles.headerBurger : styles.headerBurgerOpened}
              type="button"
              onClick={() => setIsMenuOpen(!isMenuOpen)}
            >
              <div className={styles.headerBurgerLine}>&nbsp;</div>
            </button>
            {isMenuOpen && <Navigation isOpen={isMenuOpen} />}
          </div>
        </div>
      </div>
    </header>
  );
}
