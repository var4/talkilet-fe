import iconRocket from '../../../../../assets/rocket-chat.svg';
import edit from '../../../../../assets/edit.svg';
import add from '../../../../../assets/plus-hexagon.svg';
import book from '../../../../../assets/book_open_cover_not_filled.svg';
import bookmark from '../../../../../assets/bookmark_not_filled.svg';
import registerBook from '../../../../../assets/register_book_pen_not_filled.svg';

export interface INavItem {
  src: string;
  text: string;
  submenu?: INavItem[];
  path?: string;
}
export const navData = [
  {
    src: iconRocket,
    text: 'Chat with bot',
    path: '/chat',
  },
  {
    src: book,
    text: 'Courses',
    subMenu: [
      {
        src: edit,
        text: 'Edit course',
      },
      {
        src: add,
        text: 'Create course',
      },
    ],
  },
  {
    src: bookmark,
    text: 'Exercises',
    subMenu: [
      {
        src: registerBook,
        text: 'Fill blanks',
      },
    ],
  },
];
