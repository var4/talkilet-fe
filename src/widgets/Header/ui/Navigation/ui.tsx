import React, { useState } from 'react';
import classNames from 'classnames';
import styles from './styles.module.scss';
import { navData } from './lib/navData';
import { NavigationTab } from '../NavigationTab';

interface INavigationProps {
  isOpen: boolean;
}

export function Navigation({ isOpen }: INavigationProps) {
  const [isSubmenuOpened, setIsSubmenuOpened] = useState(false);

  const navWrapperStyles = classNames({
    [styles.navWrapper]: true,
    [styles.navWrapperOpened]: isOpen,
  });

  return (
    <nav className={navWrapperStyles}>
      <div className={styles.navigation}>
        {navData.map((item) => (
          <NavigationTab
            key={item.text}
            src={item.src}
            text={item.text}
            submenu={item.subMenu}
            path={item.path}
            isSubmenuOpened={isSubmenuOpened}
            setIsSubmenuOpened={setIsSubmenuOpened}
          />
        ))}
      </div>
    </nav>
  );
}
