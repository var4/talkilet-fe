import React, { useState } from 'react';
// import { NavLink } from 'react-router-dom';
import styles from './styles.module.scss';
import arrow from '../../../../assets/arrow_right.svg';
import { Navigation } from '../Navigation/ui';
import { INavItem } from '../Navigation/lib/navData';

interface INavigationTabProps {
  src: string;
  text: string;
  submenu?: INavItem[];
  path?: string;
  isSubmenuOpened: boolean;
  setIsSubmenuOpened: (isOpened: boolean) => void;
}

export function NavigationTab({
  src,
  text,
  submenu,
  path,
  isSubmenuOpened,
  setIsSubmenuOpened,
}: INavigationTabProps) {

  const handleOnClick = () => {
    if (submenu) {
      setIsSubmenuOpened(true);
    }
  };

  return (
    <>
      {/* {path && (
        <NavLink key={path} to={path}>
          {text}
        </NavLink>
      )} */}
      {!path && (
        <>
          <button type="button" className={styles.navTab} onClick={handleOnClick}>
            <img src={src} alt="" className={styles.navIcon} />
            <p className={styles.navText}>{text}</p>
            <div className={styles.arrowWrapper}>
              <img className={styles.arrowIcon} alt="" src={arrow} />
            </div>
          </button>
          {isSubmenuOpened && <Navigation isOpen={isSubmenuOpened} />}
        </>
      )}
    </>
  );
}
