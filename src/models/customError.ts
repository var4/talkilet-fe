export interface ICustomError extends Error {
  status: number;
  statusText?: string;
  path?: string;
}

export function CustomError(): ICustomError {
  return {
    name: '',
    message: '',
    status: 0,
    statusText: '',
    path: '',
  };
}
