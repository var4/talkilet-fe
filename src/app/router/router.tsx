import React from 'react';
import { createBrowserRouter } from 'react-router-dom';
import { HomePage } from '../../pages/HomePage';
import { Dashboard } from '../../pages/Dashboard';
import { Profile } from '../../pages/Profile';
import { EditDictionary } from '../../pages/EditDictionary';

export const router = createBrowserRouter([
  {
    path: '/',
    // loader: () => ({ message: 'Hello Data Router!' }),
    Component() {
      return <HomePage />;
    },
  },
  {
    path: '/dashboard',
    Component() {
      return <Dashboard />;
    },
  },
  {
    path: '/profile',
    Component() {
      return <Profile />;
    },
  },
  {
    path: '/editDictionary',
    Component() {
      return <EditDictionary />;
    },
  },
  // {
  //   path: '/chat',
  //   Component() {
  //     return <Chat />;
  //   },
  // },
]);
