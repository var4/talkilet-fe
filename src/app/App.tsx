import React from 'react';
import './styles.module.scss';
import { SWRConfig } from 'swr';
import { RouterProvider } from 'react-router-dom';
import { router } from './router/router';
import { Header } from '../widgets/Header';
import { Footer } from '../widgets/Footer/ui';

function App() {
  return (
    <SWRConfig value={{ shouldRetryOnError: false }}>
      <Header />
      <main className="App">
        <RouterProvider router={router} />
      </main>
      <Footer />
    </SWRConfig>
  );
}

export default App;
